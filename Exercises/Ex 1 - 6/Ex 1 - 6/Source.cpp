#include <iostream>
#include <time.h>

using namespace std;

void FillArray(int numArray[], int size)
{
	srand(time(NULL));
	for (int i = 0; i <= size; i++) {
		numArray[i] = rand() % 99 + 1;
	}
}

void PrintArray(int num[], int size)
{
	cout << "original order" << endl;
	cout << "(";
	for (int i = 0; i <= size; i++) {
		cout << num[i] << ", ";
	}
	cout << "\b\b)\n";
}

void compare(int num[], int size)
{
	int largestNumber = 0;
	for (int i = 0; i <= size; i++) {
		if (num[i] > largestNumber) largestNumber = num[i];
	}
	cout << largestNumber << " is the largest number" << endl;
}

void ascendingOrder(int given[],int size)
{
	int tempNumber = 0;
	for (int i = 0; i <= size; i++) {
		for (int a = i+1; a <= size; a++) {
			if (given[i] > given[a]) {
				tempNumber = given[i];
				given[i] = given[a];
				given[a] = tempNumber;
			}
		}
	}
}

void descendingOrder(int given[], int size)
{
	int tempNumber = 10000;
	for (int i = 0; i <= size; i++) {
		for (int a = i + 1; a <= size; a++) {
			if (given[i] < given[a]) {
				tempNumber = given[i];
				given[i] = given[a];
				given[a] = tempNumber;
			}
		}
	}
}

void Order(int num[], int size)
{
	if(num[1] < num[2]) cout << "Ascending order" << endl;
	else if (num[1] > num[2]) cout << "decending order" << endl;
	cout << "(";
	for (int i = 0; i <= size; i++) {
		cout << num[i] << ", ";
	}
	cout << "\b\b)\n";
}

int main()
{
	int numbers[10] = { 0,0,0,0,0,0,0,0,0,0 };
	FillArray(numbers, 9);
	PrintArray(numbers, 9);
	ascendingOrder(numbers, 9);
	Order(numbers, 9);
	descendingOrder(numbers, 9);
	Order(numbers,9);
	system("pause");
	return 0;
}