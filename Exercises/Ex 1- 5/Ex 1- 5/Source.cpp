#include <iostream>
#include <time.h>

using namespace std;

void FillArray(int numArray[], int size)
{
	srand(time(NULL));
	for (int i = 0; i <= size; i++) {
		numArray[i] = rand() % 99 + 1;
	}
}

void PrintArray(int num[], int size)
{
	cout << "(";
	for (int i = 0; i <= size; i++) {
		cout << num[i] << ", ";
	}
	cout << "\b\b)\n";
}

void compare(int num[], int size) 
{
	int largestNumber = 0;
	for (int i = 0; i <= size; i++) {
		if (num[i] > largestNumber) largestNumber = num[i];
	}
	cout << largestNumber << " is the largest number" << endl;
}
int main()
{
	int numbers[10] = { 0,0,0,0,0,0,0,0,0,0 };
	FillArray(numbers, 9);
	PrintArray(numbers, 9);
	compare(numbers, 9);
	system("pause");
	return 0;
}