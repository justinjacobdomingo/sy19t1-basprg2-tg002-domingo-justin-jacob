#include <iostream>
#include <time.h>

using namespace std;

void randomNumberGenerator(int size, int *number)
{
	srand(time(NULL));
	for (int i = 0; i < size; i++)
	{
		*number = rand() % 99 + 1;
		++number;
	}
}

void printStar(int *given, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << *(given + i) << ", ";
		cout << (&given+ i) << endl;
	}
	cout << endl << "Size of arrays:" << sizeof(*given) << endl;
	
	cout << endl;
}

int main()
{
	int randomNumber[20];
	int* number = randomNumber;
	int n = 20;

	
	randomNumberGenerator(n, number);
	printStar(number, n);
	system("pause");
	number = new int[n];
	printStar(number, n);

	system("pause");
	return 0;
}