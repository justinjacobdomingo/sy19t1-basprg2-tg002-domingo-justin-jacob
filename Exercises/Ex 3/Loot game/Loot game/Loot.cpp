#include <iostream>
#include <time.h>
#include <string>

using namespace std;

//not used yet
struct Item
{
	string* name;
	int* gold;
	int* coins;
};
//list of items
void generateItem(string* name, int* gold, int* temp, int* multiplier)
{
	int value = rand() % 5;
	string listItem[] = { "mithril ore", "sharp talon", "thick talon", "jellopy", "cursed stone"};
	int price[] = { 100, 50, 25, 5, 0 };
	*name = listItem[value];
	*gold = price[value];
	cout << "name:" << *name << endl;
	if (*name != "cursed stone")
	{
		cout << "gold:" << *gold << endl;
		*temp = *temp + (*gold * *multiplier);
		cout << "looted gold:" << *temp << endl;
	}


}
//asking if you want to enter the dungeon
void enteringDungeon(int* coins, bool& real)
{
	char ans = 'y';
	bool question = true;
	if (*coins <= 0)
	{
		cout << "You lost the game\n";
		system("pause");
		exit(0);
	}
	else if (*coins >= 500)
	{
		cout << "You have won the game.....you reached 500 coins\n";
		exit(0);
	}
	while (question == true)
	{

		cout << "Would you like to go in the dungeon(y/n)?(fee = 25)\n";
		cin >> ans;
		switch (ans)
		{
		case 'Y':
		case 'y':
			*coins -= 25;
			cout << *coins << endl;
			question = false;
			real = true;
			break;
		case 'N':
		case 'n':
			cout << "are you sure you want to quit(y/n)?\n";
			cin >> ans;
			switch (ans)
			{
			case 'Y':
			case 'y':
				exit(0);
				break;
			case 'N':
			case 'n':
				break;
			}
		}
	}
}
//conditions 
void continueLooting(int* tempCoins, string* name, int* gold, int* coins, int* multiplier, bool& real)
{

	char ans;
	if (*name == "cursed stone")
	{
		cout << "You got the cursed stone.... you just died due to the curse\n";
		cout << "Game over.....spawing back entrance\n";
		
		*tempCoins = 0;
		*multiplier = 1;
		real = false;
	}
	else if (*coins >= 500)
	{
		cout << "You have won the game.....you reached 500 coins\n";
		exit(0);
	}
	else
	{
		cout << "Would you like to continue looting?:(y/n)\n ";
		cin >> ans;
		switch (ans)
		{
			case'y':
			case'Y':
				*multiplier = *multiplier + 1;
				break;
			case 'N':
			case 'n':
				*multiplier = 1;
				*coins += *tempCoins;
				*tempCoins = 0;
				real = false;
				break;
		}
	}
	
}

int main() 
{
	srand(time(NULL));
	int gold;
	string name;
	int coins;
	int tempCoins;
	int multiplier;
	bool alive =true;

	int* ptrGold = &gold;
	*ptrGold = 100;
	string* ptrName = &name;
	*ptrName = "yo";
	int* ptrCoins = &coins;
	*ptrCoins = 50;
	int* ptrTempCoins = &tempCoins;
	*ptrTempCoins = 0;
	int* ptrMultiplier = &multiplier;
	*ptrMultiplier = 1;




	while (true)
	{
		enteringDungeon(ptrCoins,alive);
		while (alive == true)
		{
			generateItem(ptrName, ptrGold, ptrTempCoins, ptrMultiplier);
			continueLooting(ptrTempCoins, ptrName, ptrGold, ptrCoins, ptrMultiplier, alive);
			
			system("pause");
			system("cls");
		}
	}
	
	system("pause");
	return 0;
}