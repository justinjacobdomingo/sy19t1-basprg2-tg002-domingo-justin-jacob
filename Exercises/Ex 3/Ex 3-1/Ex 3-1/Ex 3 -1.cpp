#include <iostream>
#include <time.h>

using namespace std;

void arrayPointers(int* test, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << *(test + i) << ", ";
	}
}

int main()
{
	int numbers[5] = { 1, 2, 3, 4, 5 };
	int* arrayOne = numbers;
	arrayPointers(arrayOne,5);
	system("pause");
	return 0;
}