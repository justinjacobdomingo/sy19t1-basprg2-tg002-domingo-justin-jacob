#include <iostream>
#include <string>

using namespace std;

void printArray() 
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	for (int i = 0; i <= 7; i++)
		cout << items[i] << ", ";
	cout << endl;
}

int main()
{
	printArray();
	system("pause");
	return 0;
}