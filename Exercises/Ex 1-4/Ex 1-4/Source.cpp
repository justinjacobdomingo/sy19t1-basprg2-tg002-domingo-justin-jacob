#include <iostream>
#include <time.h>

using namespace std;

void FillArray(int numArray[],int size)
{
	srand(time(NULL));
	for (int i = 0; i <= size; i++) {
		numArray[i] = rand() % 99 + 1;
	}
}
void PrintArray(int num[], int size)
{
	cout << "(";
	for(int i = 0; i <= size; i++) {
		cout << num[i] << ", ";
	}
	cout << "\b\b)\n";
}
int main()
{
	int numbers[10] = {0,0,0,0,0,0,0,0,0,0};
	FillArray(numbers,9);
	PrintArray(numbers,9);
	system("pause");
	return 0;
}