#include <iostream>
#include <string>
#include <time.h>

using namespace std;

void intro(string& name)
{
	cout << "what is your name?\nAnswer:";
	cin >> name;
}
//Ex 2 - 1 (bet);
int bet(int& cash, int gold)
{
	cout << "Gold:" << gold << endl;
	cout << "how much would you like to bet" << endl;
	cin >> cash;
	return cash;
}

//Ex 2 - 2 (diceRoll);
int diceRoll(int& diceA, int& diceB)
{
	//get 2 random number
	diceA = rand() % 6 + 1;
	diceB = rand() % 6 + 1;
	return diceA && diceB;
}

void displayDice(int diceA, int diceB,string name)
{
	//display dice
	cout << name << "' dice\n";
	cout << "First dice: " << diceA << endl;
	cout << "Second dice: " << diceB << endl << endl;
}

//Ex 2 -3(Payout)
int Payout(int pDiceA, int pDiceB, int dDiceA, int dDiceB, int cash, int& gold)
{
	int pSum = pDiceA + pDiceB;
	int dSum = dDiceA + dDiceB;
	//snake eyes
	if (pDiceA == 1 && pDiceB == 1)
	{
		cout << "\nPlayer got snake eyes" << endl;
		gold = gold + (cash * 3);
		cout << pSum << ", " << dSum << endl;
		cout << pDiceA << " + " << pDiceB << ", " << dDiceA << " + " << dDiceB << endl;
	}
	else if (pSum > dSum)
	{
		cout << "\nPlayer wins" << endl;
		gold = gold + cash;
		cout << pSum << ", " << dSum << endl;
		cout << pDiceA << " + " << pDiceB << ", " << dDiceA << " + " << dDiceB << endl;
	}
	else if (pSum == dSum)
	{
		cout << "\ndraw" << endl;
		cout << pSum << ", " << dSum << endl;
		cout << pDiceA << " + " << pDiceB << ", " << dDiceA << " + " << dDiceB << endl;
	}
	else
	{
		cout << "\nPlayerlost" << endl;
		gold = gold - cash;
		cout << pSum << ", " << dSum << endl;
		cout << pDiceA << " + " << pDiceB << ", " << dDiceA << " + " << dDiceB << endl;
	}
	return gold;
}

//Ex - 4(Playround)
bool Playround(bool& condition, int gold)
{
	if (gold <= 0)
	{
		cout << "Game Over" << endl;
		condition = false;
	}
	else
	{
		condition = true;
	}
	return condition;
}

int main()
{
	int playerA, playerB, dealerA, dealerB;
	string playerName = "hey";
	const string dealerName = "dealer";
	bool money = true;
	int cash = 0;
	int gold = 1000;
	intro(playerName);
	while (money == true)
	{
		bet(cash,gold);
		srand(time(NULL));
		//player
		diceRoll(playerA, playerB);
		displayDice(playerA, playerB,playerName);
		//dealer
		diceRoll(dealerA, dealerB);
		displayDice(dealerA, dealerB, dealerName);
		Payout(playerA, playerB, dealerA, dealerB, cash, gold);
		Playround(money, gold);
		system("pause");
		system("cls");
	}
}