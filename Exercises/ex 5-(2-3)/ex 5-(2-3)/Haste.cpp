#include "Haste.h"
#include <iostream>
#include <string>

using namespace std;

Haste::Haste()
{
	sName = "blank";
}

Haste::Haste(string name)
{
	sName = name;
}

string Haste::skillUsed()
{
	cout << "used " << sName << endl;
	return sName;
}

int Haste::gainBuff()
{
	sAgi += 2;
	return sAgi;
}
