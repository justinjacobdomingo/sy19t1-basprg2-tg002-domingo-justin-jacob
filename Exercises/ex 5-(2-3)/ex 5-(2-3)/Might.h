#pragma once
#include <string>

using namespace std;

class Might
{
public:
	Might();
	Might(string name);
	virtual string skillUsed();
	virtual int gainBuff();
private:
	string sName;
	int sPow;
};

