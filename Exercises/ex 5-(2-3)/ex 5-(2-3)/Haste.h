#pragma once
#include <string>

using namespace std;

class Haste
{
public:
	Haste();
	Haste(string name);
	virtual string skillUsed();
	virtual int gainBuff();

private:
	string sName;
	int sAgi;
	
};

