#include "Heal.h"
#include <iostream>

using namespace std;

Heal::Heal()
{
	sName = "blank";
}

Heal::Heal(string name)
{
	sName = name;
}

string Heal::skillUsed()
{
	cout << sName << " used" << endl;
	return sName;
}

int Heal::gainBuff()
{
	sHp += 10;
	return sHp;
}
