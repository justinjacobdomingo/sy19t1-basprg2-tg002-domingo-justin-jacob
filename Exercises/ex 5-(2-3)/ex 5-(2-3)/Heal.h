#pragma once
#include <string>

using namespace std;

class Heal
{
public:
	Heal();
	Heal(string name);
	virtual string skillUsed();
	virtual int gainBuff();
private:
	string sName;
	int sHp;
};

