#pragma once
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"
#include <string>

using namespace std;

class Skill
{
public:
	Skill();
	virtual string skillUsed();
	virtual int gainBuff();

private:
	string sName;
	int sHp;
	int sPow;
	int sVIt;
	int sDex;
	int sAgi;
}

