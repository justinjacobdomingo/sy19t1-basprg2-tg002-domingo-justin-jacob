#include "Might.h"
#include <iostream>
#include <string>

using namespace std;

Might::Might()
{
	sName = "blank";
}

Might::Might(string name)
{
	sName = name;
}

string Might::skillUsed()
{
	cout << "used " << sName << endl;
	return sName;
}

int Might::gainBuff()
{
	sPow += 2;
	return sPow;
}
