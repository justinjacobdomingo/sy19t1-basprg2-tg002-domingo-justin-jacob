
#include "unit.h"
#include <vector>

using namespace std;

unit::unit()
{
	uName = "blank";
	sHp = 0;
	sPow = 0;
	sVit = 0;
	sDex = 0;
	sAgi = 0;
}

unit::unit(string name, int hp, int pow, int vit, int dex, int agi, vector<Skill*> skill)
{
	uName = name;
	sHp = hp;
	sPow = pow;
	sVit = vit;
	sDex = dex;
	sAgi = agi;
	sSkill = skill;
}



string unit::skillUsed()
{
	return string();
}

int unit::gainBuff()
{
	return 0;
}
