#pragma once
#include <string>

using namespace std;

class IronSkin
{
public:
	IronSkin();
	IronSkin(string name);

	virtual string skillUsed();
	virtual int gainBuff();
private:
	string sName;
	int sVit;
};

