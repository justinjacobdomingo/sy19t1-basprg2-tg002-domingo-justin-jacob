#pragma once
#include "Skill.h"
#include <vector>
#include <string>

using namespace std;

class unit
{
public:
	unit();
	unit(string name,int hp, int pow, int vit, int dex, int agi, vector<Skill*> skill);
	
	virtual string skillUsed();
	virtual int gainBuff();
	
private:
	string uName;
	int sHp;
	int sPow;
	int sVit;
	int sDex;
	int sAgi;
	vector<Skill*> sSkill;

};

