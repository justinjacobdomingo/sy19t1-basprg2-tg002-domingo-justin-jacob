#pragma once
#include <string>

using namespace std;

class Concentration
{
public:
	Concentration();
	Concentration(string name);

	virtual string skillUsed();
	virtual int gainBuff();

private:
	string sName;
	int sDex;

};

