#include "Concentration.h"
#include <iostream>
#include <string>

using namespace std;

Concentration::Concentration()
{
	sName = "blank";
}

Concentration::Concentration(string name)
{
	sName = name;
}

string Concentration::skillUsed()
{
	cout << "used " << sName;
	return sName;
}

int Concentration::gainBuff()
{
	sDex += 2;
	return sDex;
}
