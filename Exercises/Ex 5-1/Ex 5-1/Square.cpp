#include "Square.h"
#include <iostream>

Square::Square()
{
	sName = "blank";
}

Square::Square(string name)
{
	sName = name;
}

string Square::getName()
{
	return sName;
}

int Square::getArea()
{
	return sArea;
}

int Square::getSide()
{
	return sSide;
}

int Square::setSides(int given)
{
	sSide = given;
	return sSide;
}

int Square::solveArea()
{
	sArea= sSide * sSide;
	return sSide;
}

void Square::viewStats()
{
	cout << "Name:" <<sName << endl;
	cout << "sides:" << sSide << endl;
	cout << "Area:" << sArea << endl;

}
