#pragma once
#include <string>
#include "shape.h"

using namespace std;

class rectangle : public shape
{
public:
	rectangle();
	rectangle(string name);
	virtual string getName() override;
	virtual int getArea() override;
	int getLength();
	int getWidth();

	int setLength(int given);
	int setwidth(int given);

	int solveArea();

	virtual void viewStats() override;

protected:
	string sName;
	int sArea;
	int sLength;
	int sWidth;
};

