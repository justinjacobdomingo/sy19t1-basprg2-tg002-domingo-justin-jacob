#pragma once
#include <string>
#include "shape.h"

using namespace std;

class Circle : public shape
{
public:
	Circle();
	Circle(string name);
	virtual string getName() override;
	virtual int getArea() override;
	int getRadius();
	int setRadius(int radius);

	int solveArea();
	virtual void viewStats() override;

protected:
	string sName;
	int sRadius;
	int sArea;
};

