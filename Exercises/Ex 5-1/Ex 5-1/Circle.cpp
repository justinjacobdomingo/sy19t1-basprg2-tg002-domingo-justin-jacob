#include "Circle.h"
#include "Square.h"

#include <iostream>

Circle::Circle()
{
	sRadius = 0;
	sArea = 0;
}

Circle::Circle(string name)
{
	sName = name;
}

string Circle::getName()
{
	return sName;
}

int Circle::getArea()
{
	return sArea;
}

int Circle::getRadius()
{
	return sRadius;
}

int Circle::setRadius(const int radius)
{
	sRadius = radius;
	return sRadius;
}

int Circle::solveArea()
{
	sArea = (3.14f) * (sRadius) * (sRadius);
	return sArea;
}

void Circle::viewStats()
{
	cout << "name: " << sName <<endl;
	cout << "radius:" << sRadius << endl;
	cout << "area:" << sArea << endl;
}

