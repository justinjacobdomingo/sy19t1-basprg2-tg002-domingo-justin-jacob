#pragma once
#include <string>
#include "shape.h"

using namespace std;

class Square :public shape
{
public:
	Square();
	Square(string name);

	string getName();
	virtual int getArea() override;
	int getSide();

	int setSides(int given);
	int solveArea();

	virtual void viewStats() override;
	

protected:
	string sName;
	int sSide;
	int sArea;
};

