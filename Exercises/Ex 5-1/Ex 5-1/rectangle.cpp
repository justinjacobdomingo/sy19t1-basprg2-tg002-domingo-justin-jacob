#include "rectangle.h"
#include "shape.h"
#include <iostream>

rectangle::rectangle()
{
	sName = "blank";
}

rectangle::rectangle(string name)
{
	sName = name;
}

string rectangle::getName()
{
	return sName;
}

int rectangle::getArea()
{
	return sArea;
}

int rectangle::getLength()
{
	return sLength;
}

int rectangle::getWidth()
{
	return sWidth;
}

int rectangle::setLength(int given)
{
	sLength = given;
	return sLength;
}

int rectangle::setwidth(int given)
{
	sWidth = given;
	return sWidth;
}

int rectangle::solveArea()
{
	sArea = sLength * sWidth;
	return sArea;
}

void rectangle::viewStats()
{
	cout << "Name:" << sName << endl;
	cout << "Length:" << sLength << endl;
	cout << "Width:" << sWidth << endl;
	cout << "Area" << sArea << endl;

}


