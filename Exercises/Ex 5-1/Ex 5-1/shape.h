#pragma once
#include <string>

using namespace std;

class shape
{
public:
	
	virtual string getName() = 0;
	virtual int getArea() = 0;

	virtual void viewStats() = 0;
};

