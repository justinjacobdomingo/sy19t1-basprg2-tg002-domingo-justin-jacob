#include <iostream>
#include <string>
#include  <vector>

#include "Circle.h"
#include "rectangle.h"
#include "Square.h"
#include "shape.h"

using namespace std;

vector<shape*> printVector(const vector<shape*>& listShapes)
{
	for (int i = 0; i != listShapes.size(); i++)
	{
		shape* shapeGiven = listShapes[i];
		shapeGiven->viewStats();
		cout << endl;
	}
	return listShapes;
}

int main()
{
	vector<shape*> shapeVector;
	
	Circle* basicCircle = new Circle("Circle");
	basicCircle->setRadius(5);
	basicCircle->solveArea();
	shapeVector.push_back(basicCircle);

	rectangle* basicRectangle = new rectangle("Rectangle");
	basicRectangle->setLength(5);
	basicRectangle->setwidth(4);
	basicRectangle->solveArea();
	shapeVector.push_back(basicRectangle);

	Square* basicSqauare = new Square("Square");
	basicSqauare->setSides(4);
	basicSqauare->solveArea();
	shapeVector.push_back(basicSqauare);

	printVector(shapeVector);

	
	system("pause");
	return 0;
}