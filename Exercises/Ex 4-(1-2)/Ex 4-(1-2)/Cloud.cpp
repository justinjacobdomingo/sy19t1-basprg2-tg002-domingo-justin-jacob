#include "Cloud.h"
#define Cloud_H
#include <string>
#include <iostream>

using namespace std;

Cloud::Cloud()
{
	cName = "blank";
	cLevel = 0;
	cHp = 0;
	cMp = 0;
	cStr = 0;
	cDex = 0;
	cVit = 0;
	cMgc = 0;
	cSpt = 0;
	cLck = 0;
	cAtk = 0;
	cAtkPer = 0;
	cDef = 0;
	cDefPer = 0;
	cMgcAtk = 0;
	cMgcDef = 0;
	cMgcDefPer = 0;
	cSkill1 = "blank";
	cSkill2 = "blank";
	cSkill3 = "blank";
	cSkill4 = "blank";
	cWpn = "blank";
	cArm = "blank";
	cAcc = "blank";
}

Cloud::Cloud(string name, int level, int hp, int mp, int str, int dex, int vit, int mgc, int spt, int lck, int atk, int atkPer, int def, int defPer, int mgcAtk, int mgcDef, int mgcDefPer, string skill1, string skill2, string skill3, string skill4, string wpn, string arm, string acc)
{
	cName = name;
	cLevel = level;
	cHp = hp;
	cMp = mp;
	cStr = str;
	cDex = dex;
	cVit = vit;
	cMgc = mgc;
	cSpt = spt;
	cLck = lck;
	cAtk = atk;
	cAtkPer = atkPer;
	cDef = def;
	cDefPer = defPer;
	cMgcAtk = mgcAtk;
	cMgcDef = mgcDef;
	cMgcDefPer = mgcDefPer;
	cSkill1 = skill1;
	cSkill2 = skill2;
	cSkill3 = skill3;
	cSkill4 = skill4;
	cWpn = wpn;
	cArm = arm;
	cAcc = acc;
}

void Cloud::viewstats()
{
	cout << "Name: " << cName << endl;
	cout << "Level: " << cLevel << endl;
	cout << "Hp: " << cHp << endl;
	cout << "Mp: " << cMp << endl << endl;
	cout << "Str: " << cStr << endl;
	cout << "Dex: " << cDex << endl;
	cout << "Vit: " << cVit << endl;
	cout << "Magic: " << cMgc << endl;
	cout << "Spirit: " << cSpt << endl;
	cout << "Luck: " << cLck << endl << endl;
	cout << "Attack: " << cAtk << endl;
	cout << "Attack%: " << cAtkPer << endl;
	cout << "Defence: " << cDef << endl;
	cout << "Defence%: " << cDefPer << endl;
	cout << "Magic Attack: " << cMgcAtk << endl;
	cout << "Magic Defence: " << cMgcDef << endl;
	cout << "Magic Defence%: " << cMgcDefPer << endl << endl;
	cout << "skills" << endl;
	cout << cSkill1 << endl;
	cout << cSkill2 << endl;
	cout << cSkill3 << endl;
	cout << cSkill4 << endl << endl;
	cout << "Wpn: " << cWpn << endl;
	cout << "Arm: " << cArm << endl;
	cout << "Acc: " << cAcc << endl;
}
