#pragma once
#include <string>
using namespace std;

#ifndef Cloud_H
#define Cloud_H

class Cloud
{
public:
	//constructors
	Cloud();
	Cloud(string name, int level, int hp, int mp, int str, int dex, int vit, int mgc, int spt, int lck, int atk, int atkPer, int def, int defPer, int mgcAtk, int mgcDef, int mgcDefPer, string skill1, string skill2, string skill3, string skill4, string wpn, string arm, string acc);
	void viewstats();
private:
	string cName;
	int cLevel;
	int cHp;
	int cMp;
	int	cStr;
	int cDex;
	int cVit;
	int cMgc;
	int cSpt;
	int cLck;
	int cAtk;
	int cAtkPer;
	int cDef;
	int cDefPer;
	int cMgcAtk;
	int cMgcDef;
	int cMgcDefPer;
	string cSkill1;
	string cSkill2;
	string cSkill3;
	string cSkill4;
	string cWpn;
	string cArm;
	string cAcc;
};

#endif // Cloud_H