#pragma once
#include <string>

using namespace std;

class Spell
{
public:
	Spell();
	~Spell();
	Spell(string name, int dmg, int mpCost);

	int spellDmg();
	int spellMpCost();
	string spellName();

private:
	string sName;
	int sMpCost;
	int sDmg;
};

