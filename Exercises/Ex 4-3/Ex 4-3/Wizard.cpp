#include "Wizard.h"
#include "Spell.h"
#include <string>
#include <iostream>

using namespace std;


Wizard::Wizard()
{
	wName = "Default";
	wHp = 0;
	wMp = 0;
}


Wizard::~Wizard()
{
}

Wizard::Wizard(string name, int hp, int mp)
{
	wName = name;
	wHp = hp;
	wMp = mp;
}

int Wizard::wizardHp()
{
	return wHp;
}

int Wizard::wizardMp()
{
	return wMp;
}

string Wizard::wizardName()
{
	return wName;
}

int Wizard::getDmg(Spell * cast)
{
	wHp = wHp - cast->spellDmg();
	return wHp;
}

int Wizard::reduceMp(Spell * castMp)
{
	wMp = wMp - castMp->spellMpCost();
	return wMp;
}

void Wizard::showstats()
{
	cout << wizardName() << "'s Hp: " << wHp << endl;
	cout << wizardName() << "'s Mp: " << wMp << endl;
}
