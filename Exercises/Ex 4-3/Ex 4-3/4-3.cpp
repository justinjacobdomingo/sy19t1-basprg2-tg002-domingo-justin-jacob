#include "Wizard.h"
#include "Spell.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	srand(time(NULL));
	Wizard* player = new Wizard("zuko", 100, 100);
	Wizard* enemy = new Wizard("Azula", 100, 100);
	Spell* fireball = new Spell("fireball", 10, 5);

	while(player->wizardHp() != 0 && player->wizardMp() != 0 && enemy->wizardHp() != 0 && enemy->wizardMp() != 0)
	{
		cout << "player: " << player->wizardName() << endl;
		cout << "enemy: " << enemy->wizardName() << endl;
		//player
		int x = (rand() % 99 + 1);
		if (x >= 30) 
		{
			cout << player->wizardName() << "cast a " << fireball->spellName() << " on " << enemy->wizardName() << endl;
			enemy->getDmg(fireball);
			player->reduceMp(fireball);
		}
		else
		{
			cout << player->wizardName() << " missed\n";
		}
		x = rand() % 99 + 1;
		if (x >= 30)
		{
			//enemy turn
			cout << enemy->wizardName() << "cast a " << fireball->spellName() << " on " << player->wizardName() << endl;
			player->getDmg(fireball);
			enemy->reduceMp(fireball);
		}
		else
		{
			cout << enemy->wizardName() << " missed\n";
		}
		system("pause");
		player->showstats();
		enemy->showstats();
		system("pause");
		system("cls");
		if (player->wizardHp() == 0)
		{
			cout << enemy->wizardName() << " won " << endl;
			system("pause");
		}
		if (enemy->wizardHp() == 0)
		{
			cout << player->wizardName() << " won " << endl;
			system("pause");
		}
	}
	return 0;
}