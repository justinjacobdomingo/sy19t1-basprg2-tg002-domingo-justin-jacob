#include "Spell.h"



Spell::Spell()
{
	sName = " default";
	sDmg = 0;
	sMpCost = 0;
}


Spell::~Spell()
{
}

Spell::Spell(string name, int dmg, int mpCost)
{
	sName = name;
	sDmg = dmg;
	sMpCost = mpCost;
}

int Spell::spellDmg()
{
	return sDmg;
}

int Spell::spellMpCost()
{
	return sMpCost;
}

string Spell::spellName()
{
	return sName;
}
