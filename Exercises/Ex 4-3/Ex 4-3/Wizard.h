#pragma once
#include <string>

using namespace std;

class Spell;
class Wizard
{
public:
	Wizard();
	~Wizard();
	Wizard(string name, int hp, int mp);

	int wizardHp();
	int wizardMp();
	string wizardName();

	int getDmg(Spell* cast);
	int reduceMp(Spell* castMp);
	void showstats();
private:
	string wName;
	int wHp;
	int wMp;
	Spell* spell;
};

