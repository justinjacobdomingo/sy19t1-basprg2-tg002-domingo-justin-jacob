#include <iostream>
#include <time.h>
#define NODE_H
#include <string>
#include <conio.h>

using namespace std;

struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};

bool continueGame(int count)
{
	if (count < 2)
		return false;
	else
		return true;
}

string introduction(string* playerGiven)
{
	cout << "before everything, please input your name\n";
	cin >> *playerGiven;
	cout << "Commander " << *playerGiven << ", Welcome to THE WALL.... we are currently being overhelmed by the night walkers\n";
	_getch();
	cout << "Sad to hear that you must randomly choose your top soldiers of our night watch\n";
	_getch();
	cout << "each time they would got the cloak will be chosen to defend the wall\n";
	_getch();
	cout << "The last person would move out and seek for help\n";
	_getch();
	cout << "Good luck Commander " << *playerGiven << " enjoy\n\n";
	_getch();
	return *playerGiven;
}

void createFirstNode(Node *&first,Node *&last, string given)
{
	Node* temp = new Node();
	temp->name = given;
	temp->next = NULL;
	first = temp;
	last = temp;
}

void createNewNode(Node*& first, Node*& last, string given)
{
	if (first == NULL)
		createFirstNode(first, last, given);
	else
	{
		Node* temp = new Node();
		temp->name = given;
		temp->next = last;
		last = temp;
	}
}

string getSoldier(string *&name)
{
	cout << "what would be your soldier's name\nName:";
	cin >> *name;
	return *name;
}

void showList(Node* given,int &count)
{	
	count = 0;
	while (given != NULL)
	{
		cout << given->name << endl;
		given = given->next;
		count++;
	}
	cout << "elite remaining: " << count << endl;
}

void randomizePick(Node* last,Node *&header,int count, int& randomNumber)
{
	int randomN = rand() % (count);
	for(int i = 0; i != randomN; i++)
	{
			last = last->next;
	}
	randomNumber = randomN;
	cout << "Picked: " << randomNumber + 1<< endl;
	cout << "chosed: " << last->name << endl;
}

void removeNode(Node*& last, Node*& header, int left, int number)
{
	Node* temp = last;
	if (number == 0)
	{
		last = last->next;
		delete temp;
		return;
	}
	for (int i = 0; i < number-1; i++)
		temp = temp->next;
	Node* secondTemp = temp->next;
	temp->next = secondTemp->next;
	delete secondTemp;
}

void endGame(Node* last)
{
	cout << last->name << " is the last person" << endl;
	cout << last->name << " will go out and ask for help\n";
	system("pause");

}

int main()
{
	srand(time(NULL));
	Node* frontNode = NULL;
	Node* tailNode = NULL;
	string* name = new string;
	string* playerA = new string;
	*playerA = "justin";
	int countLeft = 1;
	int soldierCount = 2;

	introduction(playerA);
	delete playerA;
	//create list
	for(int i = 0;i <= 4;i++)
		createNewNode(frontNode, tailNode, getSoldier(name));
	system("cls");
	//progress
	while (continueGame(soldierCount))
	{
		system("cls");
		showList(tailNode, soldierCount);
		//random pick
		randomizePick(tailNode,frontNode,soldierCount,countLeft);
		
		removeNode(tailNode, frontNode, soldierCount, countLeft);

		showList(tailNode, soldierCount);
		system("pause");
	}
	endGame(tailNode);
	return 0;
}