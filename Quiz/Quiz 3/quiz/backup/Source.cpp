#include <iostream>
#include <string>
#include <time.h>
#include "charClasses.h"
#include "charStats.h"
#include "character.h"

using namespace std;

int main()
{
	srand(time(0));
	//stat of class of player
	charStats* psWarrior = new charStats(15,15, 4, 6, 4, 3);
	charStats* psMage = new charStats(10,10, 10, 4, 3, 4);
	charStats* psAssassin = new charStats(12,12, 5, 3, 5, 6);

	//player
	charClasses* cWarrior = new charClasses("Warrior", psWarrior);
	charClasses* cMage = new charClasses("Mage", psMage);
	charClasses* cAssassin = new charClasses("Assassin", psAssassin);

	// character
	characters* player = new characters("player");
	//player ask name and get class
	player->askName();
	player->getClass(cWarrior, cMage, cAssassin);
	player->viewStats();
	system("pause");
	system("cls");
	for (int i = 0;player->viewChoosenStat()->getHp() > 0;i++)
	{
		//enemy stats and class
		characters* enemy = new characters("Enemy");
		charStats* esWarrior = new charStats(10, 10, 3, 4, 4, 2);
		charStats* esMage = new charStats(6, 6, 7, 2, 3, 4);
		charStats* esAssassin = new charStats(8, 8, 5, 3, 3, 6);
		charClasses* eWarrior = new charClasses("Warrior", esWarrior);
		charClasses* eMage = new charClasses("Mage", esMage);
		charClasses* eAssassin = new charClasses("Assassin", esAssassin);
		//increase stat every round
		if (i > 2)
		{
			enemy->increaseStatAssassin(enemy, i);
			enemy->increaseStatMage(enemy, i);
			enemy->increaseStatWarrior(enemy, i);
		}

		//enemysetclass
		enemy->randClass(eWarrior, eMage, eAssassin);
		enemy->viewStats();
		system("pause");
		system("cls");
		while (player->viewChoosenStat()->getHp() > 0 && enemy->viewChoosenStat()->getHp() > 0)
		{
			//enemy
			if (player->viewChoosenStat()->getHp() > 0)
			{
				if (enemy->hitRate(enemy, player) >= enemy->randChance())
				{
					if(player->playerStrong(enemy,player) == true)
					enemy->bonusDmg(enemy->viewChoosenStat()->decreaseHp(enemy->dmg(player, enemy)));
					else
					enemy->viewChoosenStat()->decreaseHp(enemy->dmg(player, enemy));
				}
				else
				{
					cout << player->viewName() << " missed " << endl;
				}
				enemy->viewStats();
				system("pause");
				system("cls");
			}
			//player
			if (enemy->viewChoosenStat()->getHp() > 0)
			{
				if (player->hitRate(player, enemy) >= player->randChance())
				{
					if (player->playerStrong(player, enemy) == true)
						player->bonusDmg(player->viewChoosenStat()->decreaseHp(player->dmg(enemy, player)));
					else
						player->viewChoosenStat()->decreaseHp(player->dmg(enemy, player));
				}
				else
					cout << enemy->viewName() << " missed " << endl;
				player->viewStats();
				system("pause");
				system("cls");
			}
		}
		//gameover
		if (player->viewChoosenStat()->getHp() == 0)
		{
			cout << "Game over" << endl;
			system("pause");
			return 0;
		}
		//stat bonus
		else if (enemy->viewChoosenClass()->getName() == "Warrior")
		{
			player->viewChoosenStat()->increaseMaxHp(3);
			player->viewChoosenStat()->increastVit(3);
		}
		else if (enemy->viewChoosenClass()->getName() == "Assassin")
		{
			player->viewChoosenStat()->increastDex(3);
			player->viewChoosenStat()->increaseAgi(3);

		}
		else if (enemy->viewChoosenClass()->getName() == "Mage")
		{
			player->viewChoosenStat()->increastPow(5);
		}
		else
			cout << "error" << endl;
		//bring everything normal
		player->viewChoosenStat()->increaseHp(player->viewChoosenStat()->getMaxHP()* 0.3);
		player->returnNormal();
		enemy->returnNormal();
		//save memmory
		delete enemy;
		system("pause");
		system("cls");
	}
}