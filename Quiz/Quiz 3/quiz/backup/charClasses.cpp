#include "charClasses.h"
#include "charStats.h"
#include <iostream>
#include <string>

charClasses::charClasses()
{
	cName = "default";
}
charClasses::charClasses(string name, charStats* newStat)
{
	cName = name;
	cStat = newStat;
}

string charClasses::getName()
{
	return cName;
}

charStats* charClasses::getStat()
{
	return cStat;
}

void charClasses::viewStat()
{
	cout << "Hp: " << cStat->getHp() << endl;
	cout << "Pow: " << cStat->getPow() << endl;
	cout << "Vit: " << cStat->getVit() << endl;
	cout << "Agi: " << cStat->getAgi() << endl;
	cout << "Dex: " << cStat->getDex() << endl;
}

