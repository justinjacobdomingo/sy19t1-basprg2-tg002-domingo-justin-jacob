#pragma once
#include <string>

using namespace std;
class charStats;
class charClasses
{
public:
	charClasses();
	charClasses(string name, charStats* newStat);

	string getName();
	charStats* getStat();

	void viewStat();

private:
	charStats* cStat;
	string cName;
};

