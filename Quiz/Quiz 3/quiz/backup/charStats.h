#pragma once
class charStats
{
public:
	charStats();
	~charStats();
	charStats(int hp,int maxHp, int pow, int vit, int dex, int agi);

	int getHp();
	int getMaxHP();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	int increaseHp(int add);
	int increaseMaxHp(int add);
	int increastPow(int add);
	int increastVit(int add);
	int increaseAgi(int add);
	int increastDex(int add);

	int decreaseHp(int minus);

private:
	int cHp;
	int cMaxHp;
	int cPow;
	int cVit;
	int cAgi;
	int cDex;
};

