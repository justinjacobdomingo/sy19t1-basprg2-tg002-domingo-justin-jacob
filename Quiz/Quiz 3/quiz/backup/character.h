#pragma once
#include <string>
using namespace std;
class charStats;
class charClasses;
class characters
{
public:
	//constructor
	characters();
	characters(string name);
	//player
	string askName();
	//class
	//playerClass
	charClasses* getClass(charClasses* givenA, charClasses* givenB, charClasses* givenC);
	//enemy class
	charClasses* randClass(charClasses* givenA, charClasses* givenB, charClasses* givenC);
	//view stats
	void viewStats();
	charStats* viewChoosenStat();
	charClasses* viewChoosenClass();
	string viewName();
	//formula
	int hitRate(characters* player, characters* enemy);
	int dmg(characters* player, characters* enemy);
	int bonusDmg(int attack);
	bool playerStrong(characters* player, characters* enemy);
	bool playerWeak(characters* player, characters* enemy);
	bool returnNormal();
	int randChance();
	void increaseStatMage(characters* given,int num);
	void increaseStatWarrior(characters* given,int num);
	void increaseStatAssassin(characters* given,int num);
		
private:
	string characterName;
	charClasses* chosenClass;
	charStats* chosenStat;

	bool strongAgianstPlayer;
	bool weakAgainstPlayer;

	int charHitRate;
	int charDmg;
};



