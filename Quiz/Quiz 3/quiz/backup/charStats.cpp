#include "charStats.h"
#include <iostream>

using namespace std;

charStats::charStats()
{
	cHp = 0;
	cMaxHp = 0;
	cPow = 0;
	cVit = 0;
	cAgi = 0;
	cDex = 0;
}

charStats::~charStats()
{
}

charStats::charStats(int hp, int maxHp, int pow, int vit, int dex, int agi)
{
	cHp = hp;
	cPow = pow;
	cMaxHp = maxHp;
	cVit = vit;
	cDex = dex;
	cAgi = agi;
}

int charStats::getHp()
{
	return cHp;
}

int charStats::getMaxHP()
{
	return cMaxHp;
}

int charStats::getPow()
{
	return cPow;
}

int charStats::getVit()
{
	return cVit;
}

int charStats::getAgi()
{
	return cAgi;
}

int charStats::getDex()
{
	return cDex;
}

int charStats::increaseHp(int add)
{
	cHp += add;
	return cHp;
}

int charStats::increaseMaxHp(int add)
{
	cMaxHp += add;
	return cMaxHp;
}

int charStats::increastPow(int add)
{
	cPow += add;
	return cPow;
}

int charStats::increastVit(int add)
{
	cVit += add;
	return cVit;
}

int charStats::increaseAgi(int add)
{
	cAgi += add;
	return cAgi;
}

int charStats::increastDex(int add)
{
	cDex += add;
	return cDex;
}

int charStats::decreaseHp(int minus)
{
	cHp = cHp - minus;
	if (cHp < 0)
		cHp = 0;
	return cHp;
}


