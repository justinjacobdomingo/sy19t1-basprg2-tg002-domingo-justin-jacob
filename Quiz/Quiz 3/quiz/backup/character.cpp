#include "character.h"
#include "charClasses.h"
#include "charStats.h"
#include <time.h>
#include <iostream>
#include <string>

using namespace std;

characters::characters()
{
	characterName = "default";
	strongAgianstPlayer = false;
	weakAgainstPlayer = false;
}

characters::characters(string name)
{
	characterName = name;
}

string characters::askName()
{
	cout << "player's Name: ";
	cin >> characterName;
	return characterName;
}

charClasses* characters::getClass(charClasses* givenA, charClasses* givenB, charClasses* givenC)
{
	int num = 0;
	cout << "[1] Warrior" << endl;
	cout << "[2] Mage " << endl;
	cout << "[3] Assassin" << endl;
	cout << "what Class would you like to use" << endl;
	cin >> num;
	switch (num)
	{
	case 1:
		chosenClass = givenA;
		delete givenB;
		delete givenC;
		break;
	case 2:
		chosenClass = givenB;
		delete givenA;
		delete givenC;
		break;
	case 3:
		chosenClass = givenC;
		delete givenA;
		delete givenB;
		break;
	}
	chosenStat = chosenClass->getStat();
	return chosenClass;
}

charClasses* characters::randClass(charClasses* givenA, charClasses* givenB, charClasses* givenC)
{
	int randNum = rand() % 3;
	if (randNum == 2) chosenClass = givenA;
	else if (randNum == 1) chosenClass = givenB;
	else if (randNum == 0)chosenClass = givenC;
	chosenStat = chosenClass->getStat();
	return chosenClass;
}

void characters::viewStats()
{
	cout << characterName << endl;
	cout << chosenClass->getName() << endl;
	cout << "Hp: " << chosenStat->getHp() << endl;
	cout << "Pow: " << chosenStat->getPow() << endl;
	cout << "Vit: " << chosenStat->getVit() << endl;
	cout << "Agi: " << chosenStat->getAgi() << endl;
	cout << "Dex: " << chosenStat->getDex() << endl;
}

charStats* characters::viewChoosenStat()
{
	return chosenStat;
}

charClasses* characters::viewChoosenClass()
{
	return chosenClass;
}

string characters::viewName()
{
	return characterName;
}

int characters::hitRate(characters* player, characters* enemy)
{
	charHitRate = (player->chosenStat->getDex() / enemy->chosenStat->getAgi() * 100);
	return charHitRate;
}

int characters::dmg(characters* player, characters* enemy)
{
	charDmg = player->viewChoosenStat()->getPow() - enemy->viewChoosenStat()->getVit();
	if (charDmg <= 0)
		charDmg = rand() % player->viewChoosenStat()->getPow() - rand() % enemy->viewChoosenStat()->getVit();
	if (charDmg <= 0)
		charDmg = -(charDmg);
	cout << enemy->characterName << " got " << charDmg << " dmg" << endl;
	return charDmg;
}

int characters::bonusDmg(int attack)
{
	charDmg = attack + (attack / 2);
	return charDmg;
}

bool characters::playerStrong(characters* player, characters* enemy)
{
	if (player->chosenClass->getName() == "Warrior" && enemy->chosenClass->getName() == "Assassin")
		strongAgianstPlayer = true;
	if (player->chosenClass->getName() == "Mage" && enemy->chosenClass->getName() == "Warrior")
		strongAgianstPlayer = true;
	if (player->chosenClass->getName() == "Asssassin" && enemy->chosenClass->getName() == "Mage")
		strongAgianstPlayer = true;
	return strongAgianstPlayer;
}

bool characters::playerWeak(characters* player, characters* enemy)
{
	if (player->chosenClass->getName() == "Warrior" && enemy->chosenClass->getName() == "Mage")
		weakAgainstPlayer = true;
	if (player->chosenClass->getName() == "Mage" && enemy->chosenClass->getName() == "Assassin")
		weakAgainstPlayer = true;
	if (player->chosenClass->getName() == "Asssassin" && enemy->chosenClass->getName() == "Warrior")
		weakAgainstPlayer = true;
	return weakAgainstPlayer;
}

bool characters::returnNormal()
{
	strongAgianstPlayer = false;
	return strongAgianstPlayer;
}

int characters::randChance()
{
	return rand() % 20;
}

void characters::increaseStatMage(characters* given,int num)
{
	given->viewChoosenStat()->increastPow(5+num);
}

void characters::increaseStatWarrior(characters* given, int num)
{
	given->viewChoosenStat()->increaseHp(3+num);
	given->viewChoosenStat()->increaseMaxHp(3 + num);
	given->viewChoosenStat()->increastVit(3 + num);
}

void characters::increaseStatAssassin(characters* given, int num)
{
	given->viewChoosenStat()->increaseAgi(3 + num);
	given->viewChoosenStat()->increastDex(3 + num);
}

