#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

void start()
{
	cout << "Welcome to IAP test area" << endl;
	cout << "This application would test out the IAP(In Apps purchases mechanics)" << endl;
	cout << "where in, it would suggest a packages if the player would lack money for the inputed item" << endl;
	cout << "This is more of an simulation of what IAP would do\n\n";
}
//intro
void stats(int money)
{
	cout << "Player's Gold:" << money << endl;
}
//shows money
int userInputPrice(int inputPrice)
{
	cout << "How much is the item?:";
	cin >> inputPrice;
	return inputPrice;
}
//ask for price
int sortArray(int givenArray[], int size)
{
	int tempValue = 0;
	for (int i = 0; i <= size; i++)
	{
		for (int a = i + 1; a <= size; a++)
		{
			if (givenArray[i] > givenArray[a])
			{
				tempValue = givenArray[i];
				givenArray[i] = givenArray[a];
				givenArray[a] = tempValue;
			}
		}
	}
	return givenArray[6];
}
int deductionIAP(int money, int given[], int input)
{
	int addPackage = 0;
	cout << " What package would you like?(input the number of the package):\n";
	cin >> addPackage;
	money = money + given[addPackage - 1];
	money = money - input;
	return money;
}
//used IAP
int IAPPackageList(int given[], int size, int input, int money)
{
	int addPackage = 0;
	if (money >= input)
	{
		money = money - input;
	}
	else
	{
		cout << "You lack money" << endl;
		if (input - money <= 150)//1
		{
			for (int i = 0; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given, input);
		}
		else if (input - money <= 500)//2
		{
			for (int i = 1; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given, input);
		}
		else if (input - money <= 1780)//3
		{
			for (int i = 2; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given,input);
		}
		else if (input - money <= 4050)//4
		{
			for (int i = 3; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given, input);
		}
		else if (input - money <= 13333)//5
		{
			for (int i = 4; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given,input);
		}
		else if (input - money <= 30750)//6
		{
			for (int i = 5; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given,input);
		}
		else if (input - money <= 250000)//7
		{
			for (int i = 6; i <= size; i++)
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given,input);
		}
		else
		{
			cout << "We would also like to say that we don't have the package that would help you so we would like to show you all of what we have\n";
			for (int i = 0; i <= size; i++) 
			{
				cout << "package" << i + 1 << ":" << given[i] << endl;
			}
			money = deductionIAP(money, given,input);
		}
	}
		return money;
}
//if the input is greater than or less than the money;
bool continueGame(bool condition)
{
	bool ThereInput = true;
	char inputText;
	cout << "would you like to continue(Y/N)\n";
	while (ThereInput == true)
	{
		cin >> inputText;
		switch (inputText)
		{
		case 'N':
		case 'n':
			condition = false;
			ThereInput = false;
			break;
		case 'Y':
		case 'y':
			condition = true;
			ThereInput = false;
			break;
		default:
			break;
		}
	}
	return condition;
}
//continue game
int main()
{
	int playersGold = 250, itemPrice = 0;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	bool game = true;
	start();
	while (game == true)
	{
		stats(playersGold);
		itemPrice = userInputPrice(itemPrice);
		packages[6] = sortArray(packages, 6);// sorting the array
		playersGold = IAPPackageList(packages, 6, itemPrice, playersGold);
		stats(playersGold);
		game = continueGame(game);
		system("pause");
		system("cls");
	}
	system("pause");
	return 0;
}