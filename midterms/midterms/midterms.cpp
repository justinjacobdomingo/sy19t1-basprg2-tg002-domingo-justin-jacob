#include <iostream>
#include <vector>
#include <string>
#include <time.h>

using namespace std;

void statPlayer(int cash, int ear,string side)
{
	cout << "cash: " << cash << endl;
	cout << "distance left: " << ear << endl;
	cout << "side: " << side << endl;
}

void askBet(int* bet,int* ear)
{
	bool possibleBet = false;
	while (possibleBet == false)
	{
		cout << "How much would you like to bet?\nAnswers: ";
		cin >> *bet;
		if (*bet <= *ear)
			possibleBet = true;
	}
}

void swapping(bool* swap,int* num,string* side)
{
	//swap side
	if(*num == 2 && *swap == false)
		* side = "Emperor";
	else if(*num == 2 && *swap == true)
		*side = "slave";
	//swap deck
	if (*num % 3 == 0 && *swap == false)
	{
		*swap = true;
		*num = 1;
	}
	else if (*num % 3 == 0 && *swap == true)
	{
		*swap = false;	
		*num = 1;
	}
	else
		++*num;
}

void createEmperordeck(vector<string>* given)
{
	given->clear();
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("Emperor");
}

void createSlavedeck(vector<string>* given)
{
	given->clear();
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("civillian");
	given->push_back("Slave");
}

void swapDeks(vector<string>* player, vector<string>* dealer, bool* swap)
{
	if (*swap == true)
	{
		createEmperordeck(player);
		createSlavedeck(dealer);
	}
	else
	{
		createSlavedeck(player);
		createEmperordeck(dealer);
	}
}

void printDeck(vector<string> given,string name)
{
	cout << "Turn: " << name << endl;
	
		for (int i = 0; i != given.size(); i++)
		{
			cout << "card number " << i + 1 << " : ";
			cout << given[i] << endl;
		}
	
	cout << endl << endl;
}

void playerPickCard(vector<string>* given, string* picked)
{
	bool enoughCard = true;
	int number;
	while (enoughCard == true)
	{
		cout << "choose a card:";
		cin >> number;
		if (number < given->size() + 1)
			enoughCard = false;
	}
	*picked = given->at(number - 1);
	cout << "you have chosen the " << *picked << endl;
	given->erase(given->begin() + (number - 1));
	system("cls");
}

void randomPickDealer(vector<string>* given, string* picked)
{
	int randomNumber = rand() % given->size();
	*picked = given->at(randomNumber);
	cout << "dealer have chosen the " << *picked << endl << endl;
	given->erase(given->begin() + randomNumber);
}

void toWin(string* dealer, string* player, int* value, int* cash,int* bet,bool* winnerGame)
{
	cout << "dealer: " << *dealer << endl;
	cout << "player: " << *player << endl;
	if (*dealer == "civillian" && *player == "civillian")
	{
		cout << "You got a draw" << endl;
	}
	else if (*dealer == "civillian" && *player == "Emperor")
	{
		cout << "you won" << endl;
		*cash = *cash + (*bet * 100000);
		*winnerGame = true;
		cout << "You won " << (*bet * 100000) << endl;
	}
	else if (*dealer == "Emperor" && *player == "Slave")
	{
		cout << "you won" << endl;
		*cash = *cash + (*bet * 500000);
		*winnerGame = true;
		cout << "You won " << (*bet * 100000) << endl;
	}
	else if (*dealer == "Slave" && *player == "civillian")
	{
		cout << "you won" << endl;
		*cash = *cash + (*bet * 100000);
		*winnerGame = true;
		cout << "You won " << (*bet * 100000) << endl;
	}
	else 
	{
		cout << "you lost" << endl;
		*value = *value - *bet;
		*winnerGame = true;
		cout << "You lost " << *bet << " mm" << endl;
	}
}

void ending(int* cash, int* value)
{
	if (*cash >= 20000000 && *value > 0)
		cout << "You got the best Ending congrats\nYou have avenge your fallen brothers" << endl;
	else if (*cash > 0 && *value > 0)
		cout << "You got the meh ending....... MEH" << endl << "You need to get 20,000,000 cash to get best ending" << endl;
	else
		cout << "You lost your ear, better luck next time" << endl;
}

int main()
{
	srand(time(NULL));
	vector<string> playerVector;
	vector<string> dealerVector;
	string playerCard, dealerCard;
	string sidePlayer = "Emperor";

	int playerEar = 30;
	int playerCash = 0;
	int roundN = 0;

	bool swap = false;
	bool winForRound = false;
	//used as a pointer
	int counter = 0;
	
	for (int i = 0; i != 12 && playerEar != 0; i++) 
	{
		//newValue every round
		int* betM = new int;

		cout << "Round " << i + 1 << endl << "------------------" << endl;
		//assigning deck
		statPlayer(playerCash, playerEar,sidePlayer);
		swapping(&swap,&counter,&sidePlayer);
		swapDeks(&playerVector, &dealerVector, &swap);
		//ask bet
		askBet(betM, &playerEar);
		//so it will play the round
		winForRound = false;
		while ((playerVector.size() != 0) && (dealerVector.size() != 0) && winForRound == false)
		{
			printDeck(playerVector, "player");
			//pick a card
			playerPickCard(&playerVector, &playerCard);
			//randompick dealer
			randomPickDealer(&dealerVector, &dealerCard);
			//conditions
			toWin(&dealerCard, &playerCard, &playerEar, &playerCash,betM,&winForRound);
			
			system("pause");
			system("cls");
		}
		delete betM;
	}

	ending(&playerCash, &playerEar);
	system("pause");
	return 0;
}