#include "Crystal.h"
#include "player.h"

Crystal::Crystal()
{
	iName = "Crystal";
}

void Crystal::effect(player* collector)
{
	//add pull count
	collector->addpCrystal();
	//effect
	collector->addCrystals(15);
}
