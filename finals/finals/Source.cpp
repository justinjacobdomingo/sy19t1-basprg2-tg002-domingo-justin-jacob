#include <iostream>
#include <string>
#include <vector>
#include <time.h>

#include "player.h"
#include "item.h"
#include "Machine.h"

#include"Bomb.h"
#include"Crystal.h"
#include"R.h"
#include "SR.h"
#include "SSR.h"
#include "HealthPotion.h"


using namespace std;

//shorter code
void systemCode()
{
	system("pause");
	system("cls");
}

//test reason
vector<item*> printVector(const vector<item*>& listShapes, player* given)
{
	for (int i = 0; i != listShapes.size(); i++)
	{
		item* itemGiven = listShapes[i];
		itemGiven->effect(given);
		cout << endl;
	}
	return listShapes;
}

int main()
{
	srand(time(NULL));
	//players and objects
	player* playerOne = new player();
	vector<item*> playerItem;
	Machine* pMachine = new Machine(playerOne);

	item* pBomb = new Bomb();
	item* pCrystal = new Crystal();
	item* pHealthPotion = new HealthPotion();
	item* pR = new R();
	item* pSR = new SR();
	item* pSSR = new SSR();
	
	// player set vector;
	playerOne->setVector(playerItem);
	playerOne->pushVector(pBomb);
	playerOne->pushVector(pCrystal);
	playerOne->pushVector(pHealthPotion);
	playerOne->pushVector(pR);
	playerOne->pushVector(pSR);
	playerOne->pushVector(pSSR);


	//ask name
	playerOne->askName();
	playerOne->viewStats();
	systemCode();
	//the game
	while (pMachine->continueGame())
	{
		//pick random item();
		pMachine->payment(playerOne);
		pMachine->randPick(playerOne);
		pMachine->addEffect(playerOne);
		//view stat
		playerOne->viewStats();
		systemCode();
	}
	playerOne->viewStats();
	pMachine->victoryCondition();
	systemCode();
	return 0;
}