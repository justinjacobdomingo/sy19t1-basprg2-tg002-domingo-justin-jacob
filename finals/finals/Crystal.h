#pragma once
#include <string>
#include "item.h"

using namespace std;

class player;

class Crystal:public item
{
public:
	Crystal();

	void effect(player* collector);
};

