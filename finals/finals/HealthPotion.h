#pragma once
#include <string>
#include "item.h"

using namespace std;

class player;

class HealthPotion:public item
{
public:
	HealthPotion();

	virtual void effect(player* collector);
};

