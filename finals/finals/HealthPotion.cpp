#include "HealthPotion.h"
#include "player.h"

HealthPotion::HealthPotion()
{
	iName = "HealthPotion";
}

void HealthPotion::effect(player* collector)
{
	//add pull count
	collector->addpHealthPotion();
	//effect
	collector->addHp(30);
}
