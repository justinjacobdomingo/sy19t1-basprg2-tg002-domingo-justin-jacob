#include "player.h"
#include <iostream>
#include <string>

using namespace std;

vector<item*> player::setVector(vector<item*> given)
{
	currentItem = given;
	return currentItem;
}

vector<item*> player::getVector()
{
	return currentItem;
}

vector<item*> player::pushVector(item* add)
{
	currentItem.push_back(add);
	return currentItem;
}

string player::askName()
{
	string given;
	cout << "input Name: ";
	cin >> given;
	pName = given;
	return pName;
}

string player::getPlayerName()
{
	return pName;
}

void player::viewStats()
{
	cout << "Name: " << pName << endl;

	cout << "            stats           " << endl;
	cout << "----------------------------" << endl;
	cout << "Hp: " << iHp << endl;
	cout << "Rare Points: " << iRarePoints << endl;
	cout << "Crystals: " << iCrystals << endl << endl;
	
	cout << "           Pull # "<<  pulls << endl;
	cout << "---------------------------" << endl;
	cout << "R: " << cRarePoints << endl;
	cout << "SR: " << cSRarePoints << endl;
	cout << "SSR: " << cSSRarePoints << endl;
	cout << "Bomb: " << cBomb << endl;
	cout << "Crystals: " << cCrystals << endl;
	cout << "Health Potion: " << cHealthPotions << endl << endl;


}

player::player()
{
	pName = "player";
	iHp = 100;
	iRarePoints = 0;
	iCrystals = 100;
	cRarePoints = 0;
	cSRarePoints = 0;
	cSSRarePoints = 0;
	cBomb = 0;
	cCrystals = 0;
	cHealthPotions = 0;
}

int player::getIHp()
{
	return iHp;
}

int player::getICrystal()
{
	return iCrystals;
}

int player::getIRarePoints()
{
	return iRarePoints;
}

int player::minusHp(int reducer)
{
	iHp -= reducer;
	if (iHp < 0) iHp = 0;
	return iHp;
}

int player::minusCrystals(int reducer)
{
	iCrystals -= reducer;
	if (iCrystals < 0) iCrystals = 0;
	return iCrystals;
}

int player::addCrystals(int adder)
{
	iCrystals += adder;
	return iCrystals;
}

int player::addRarityPoints(int adder)
{
	iRarePoints += adder;
	return iRarePoints;
}

int player::addHp(int adder)
{
	iHp += adder;
	return iHp;
}

int player::addPull()
{
	pulls++;
	return pulls;
}

int player::addpBomb()
{
	cBomb++;
	return cBomb;
}

int player::addpCrystal()
{
	cCrystals++;
	return cCrystals;
}

int player::addpHealthPotion()
{
	cHealthPotions++;
	return cHealthPotions;
}

int player::addpR()
{
	cRarePoints++;
	return cRarePoints;
}

int player::addpSR()
{
	cSRarePoints++;
	return cSRarePoints;
}

int player::addpSSR()
{
	cSSRarePoints++;
	return cSSRarePoints;
}

