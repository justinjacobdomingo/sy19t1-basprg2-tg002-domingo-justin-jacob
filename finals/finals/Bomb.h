#pragma once
#include <string>
#include "item.h"

using namespace std;

class player;

class Bomb :public item
{
public:
	//constructor
	Bomb();

	void effect(player* collector) override;
};
