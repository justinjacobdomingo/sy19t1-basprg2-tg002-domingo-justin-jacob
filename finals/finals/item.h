#pragma once
#include <string>
#include <vector>


using namespace std;

class player;

class item
{
public:
	item();

	// effect of item
	virtual void effect(player* collector) = 0;

protected:
	// name of item ;
	string iName;

	//stats
	int iHp;
	int iCrystals;
	int iRarePoints;

	//counts of all;
	int cRarePoints;
	int cSRarePoints;
	int cSSRarePoints;
	int cHealthPotions;
	int cBomb;
	int cCrystals;
};

