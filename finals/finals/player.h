#pragma once
#include <string>
#include <vector>

using namespace std;

class item;

class player
{
public:
	//contsructor
	player();

	//view Stat points
	int getIHp();
	int getICrystal();
	int getIRarePoints();

	//add and subtract
	//-
	int minusHp(int reducer);
	int minusCrystals(int reducer);
	//+	
	int addCrystals(int adder);
	int addRarityPoints(int adder);
	int addHp(int adder);
	
	// count pulls
	int addPull();
	int addpBomb();
	int addpCrystal();
	int addpHealthPotion();
	int addpR();
	int addpSR();
	int addpSSR();

	//add items
	vector<item*> setVector(vector<item*> given);
	vector<item*> getVector();
	vector<item*> pushVector(item* add);

	//item effect
	// playername
	//ask name
	string askName();
	string getPlayerName();

	// viewStats
	void viewStats();

private:
	string pName;
	vector<item*> currentItem;
	item* givenItem;
	int pulls;

protected:
	// name of item ;
	string iName;

	//stats
	int iHp;
	int iCrystals;
	int iRarePoints;
	//counts of all;

	int cRarePoints;
	int cSRarePoints;
	int cSSRarePoints;
	int cHealthPotions;
	int cBomb;
	int cCrystals;
};


