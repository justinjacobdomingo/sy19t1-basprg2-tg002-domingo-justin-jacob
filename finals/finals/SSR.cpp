#include "SSR.h"
#include "player.h"

SSR::SSR()
{
	iName = "SSR";
}

void SSR::effect(player* collector)
{
	//add pull count
	collector->addpSSR();
	// effect
	collector->addRarityPoints(50);
}
