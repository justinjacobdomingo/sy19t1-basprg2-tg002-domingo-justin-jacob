#include "Bomb.h"
#include "player.h"

Bomb::Bomb()
{
	this->iName = "Bomb";
}

void Bomb::effect(player* collector)
{
	//add pull count
	collector->addpBomb();
	//effect
	collector->minusHp(25);
}
