#include "R.h"
#include "player.h"

R::R()
{
	iName = "R";
}

void R::effect(player* collector)
{
	//add pull count
	collector->addpR();
	//effect
	collector->addRarityPoints(1);
}
