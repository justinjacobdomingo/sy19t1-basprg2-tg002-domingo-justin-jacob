#pragma once
#include <string>
#include <vector>

using namespace std;

class item;
class player;
class Machine
{
public:
	Machine();
	Machine(player* pGiven);
	// waht a machines do
	int payment(player* given);
	item* randPick(player* given);
	item* addEffect(player* given);

	bool continueGame();
	void victoryCondition();

private:
	bool condition;
	int paymentPrice;
	int chance;
	player* playerGiven;
	item* playerGotten;

protected:
	// name of item ;
	string iName;

	//stats
	int iHp;
	int iCrystals;
	int iRarePoints;
	//counts of all;

	int cRarePoints;
	int cSRarePoints;
	int cSSRarePoints;
	int cHealthPotions;
	int cBomb;
	int cCrystals;
};

