#include "Machine.h"
#include "player.h"
#include "item.h"
#include "Bomb.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"

#include <time.h>
#include <string.h>
#include <iostream>

using namespace std;

Machine::Machine()
{
	playerGiven = NULL;
	playerGotten = NULL;
	paymentPrice = 0;
	chance = 0;
	iHp = 100;
	iRarePoints = 0;
	iCrystals = 100;
	cRarePoints = 0;
	cSRarePoints = 0;
	cSSRarePoints = 0;
	cBomb = 0;
	cCrystals = 0;
	cHealthPotions = 0;
}

Machine::Machine(player* pGiven)
{
	playerGiven = pGiven;
	playerGotten = NULL;
	chance = 0;
	paymentPrice = 5;
	iHp = 100;
	iRarePoints = 0;
	iCrystals = 100;
	cRarePoints = 0;
	cSRarePoints = 0;
	cSSRarePoints = 0;
	cBomb = 0;
	cCrystals = 0;
	cHealthPotions = 0;
	cHealthPotions = 0;
}

int Machine::payment(player* given)
{
	given->minusCrystals(paymentPrice);
	return iCrystals;
}

item* Machine::randPick(player* given)
{
	chance = rand() % 101;
	if (chance < 20)//bomb
	{
		cout << "you got a bomb" << endl;
		playerGotten = given->getVector().at(0);
	}
	else if (chance < 35)//crystal
	{
		cout << "you got a Crystal" << endl;
		playerGotten = given->getVector().at(1);
	}
	else if (chance < 50)//healthPotion
	{
		cout << "you got a healthPotion" << endl;
		playerGotten = given->getVector().at(2);
	}
	else if (chance < 90)//R
	{
		cout << "you got a R item" << endl;
		playerGotten = given->getVector().at(3);
	}
	else if (chance < 100)//SR
	{
		cout << "you got a SR item" << endl;
		playerGotten = given->getVector().at(4);
	}
	else//SSR
	{
		cout << "you got a SSR item" << endl;
		playerGotten = given->getVector().at(5);
	}
	playerGiven->addPull();
	cout << endl;
	return playerGotten;
}

item* Machine::addEffect(player* given)
{
	playerGotten->effect(given);
	return playerGotten;
}

bool Machine::continueGame()
{
	if (playerGiven->getIHp() > 0 && playerGiven->getIRarePoints() < 100 && playerGiven->getICrystal() > 0)
		condition = true;
	else
		condition = false;
	return condition;
}

void Machine::victoryCondition()
{
	if (playerGiven->getICrystal() <= 0 || playerGiven->getIHp() <= 0)
		cout << "You didn't reach your goal\nYou lose better luck next time" << endl;
	else
		cout << "You won" << endl;
}
