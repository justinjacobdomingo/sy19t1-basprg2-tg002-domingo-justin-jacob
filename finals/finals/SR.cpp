#include "SR.h"
#include "player.h"

SR::SR()
{
	iName = "SR";
}

void SR::effect(player* collector)
{
	//add pull count
	collector->addpSR();
	//effect
	collector->addRarityPoints(10);
}
